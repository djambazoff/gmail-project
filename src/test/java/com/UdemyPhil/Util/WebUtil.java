package com.UdemyPhil.Util;

import com.UdemyPhil.pageObjects.EmailHomePage;
import com.UdemyPhil.pageObjects.SignInPage;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebUtil {
	public static SignInPage goToSignInPage(WebDriver driver){
		driver.get("http://gmail.com");
		return PageFactory.initElements(driver,SignInPage.class );
	}
	public static void click(WebDriver driver, By element){
		WebElement objectToClick = driver.findElement(element);
		objectToClick.click();
	}
	public static void waitElement(WebDriver driver, By element, int time){
		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	public static void isElementDisplayed(String message, String expected, String actual){
		Assert.assertEquals(message, expected, actual);
	}
	public static void verifyElement(WebDriver driver, By element, String message){
		Assert.assertTrue(message, driver.findElements(element).size()>0);
		
	}
	public static void inputData(WebDriver driver, By element, String data){
		WebElement toField=driver.findElement(element);
		toField.sendKeys(data);
	}
	public static String extractText(WebDriver driver, By element){
		WebElement objElement=driver.findElement(By.cssSelector("div[class='y6'] span[id] b"));
		String object=objElement.getText();
		return object;
	}
	
}
