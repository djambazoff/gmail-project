package com.UdemyPhil.pageObjects;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.UdemyPhil.Util.WebUtil;

public class SignInPage {
		WebDriverWait wait;
		
		public void fillInUsername ( WebDriver driver, String username) {
			WebUtil.waitElement(driver, By.id("Email"), 20);
			WebUtil.inputData(driver, By.id("Email"), username);
			WebUtil.click(driver, By.id("next"));
			
		}
		public void fillInPassword ( WebDriver driver, String pass) {
			WebUtil.waitElement(driver, By.id("Passwd"), 10);
			WebUtil.inputData(driver, By.id("Passwd"), pass);
			
		}
		public  EmailHomePage clickSignIn( WebDriver driver){
			WebUtil.click(driver, By.id("signIn"));
			return PageFactory.initElements(driver, EmailHomePage.class);
			}
		public void verifySignOut (WebDriver driver){
			WebUtil.waitElement(driver, By.id("signIn"), 20);
			WebUtil.verifyElement(driver, By.id("signIn"), "Element is NOT verified!");
			
		}

}

