package com.UdemyPhil.pageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.UdemyPhil.Util.WebUtil;

public class EmailHomePage {
	
	WebDriverWait wait;
	public void verifySignIn(WebDriver driver, String message) {
		WebUtil.waitElement(driver, By.partialLinkText("Inbox"), 20);
		WebUtil.verifyElement(driver, By.partialLinkText("Inbox"), message);
		//Assert.assertTrue(message,!driver.findElements(By.partialLinkText("Inbox")).isEmpty());
	}
	public void clickCompose (WebDriver driver){
		WebUtil.waitElement(driver, By.cssSelector("div[role='button'][gh='cm']"), 20);
		WebUtil.click(driver, By.cssSelector("div[role='button'][gh='cm']"));
	}
	public void fillInRecipient( WebDriver driver, String email){
		WebUtil.waitElement(driver, By.cssSelector("textarea[name='to']"), 20);
		WebUtil.inputData(driver,By.cssSelector("textarea[name='to']"), email);
		
	}
	public void inputSubject(WebDriver driver, String subject){
		WebElement subjectEmail=driver.findElement(By.cssSelector("input[name='subjectbox']"));
		subjectEmail.sendKeys(subject);
	}
	public void fillInBody(WebDriver driver, String text ){
		WebElement textEmail=driver.findElement(By.cssSelector("div[role='textbox']"));
		textEmail.sendKeys(text);
	}
	public void sendEmail( WebDriver driver){
		WebElement sendEmail=driver.findElement(By.cssSelector("div[data-tooltip*='Send']"));
		sendEmail.click();
	}
	public void checkInboxStatus(WebDriver driver){
		WebUtil.waitElement(driver, By.linkText("Inbox (1)"), 20);
		WebUtil.click(driver, By.linkText("Inbox (1)"));
	}
	public String getActualSubject(WebDriver driver){
		WebUtil.waitElement(driver, By.cssSelector("div[class='y6'] span[id] b"), 20);
		return WebUtil.extractText(driver,By.cssSelector("div[class='y6'] span[id] b"));
	}
	public String getActualBody(WebDriver driver){
		WebUtil.waitElement(driver, By.cssSelector("div[class='y6'] span[id] b"), 20);
		WebElement newEmailBody=driver.findElement(By.cssSelector("div[class='y6'] span[class='y2']"));
		String bodyRec=newEmailBody.getText().replace('-', ' ').trim();
		return bodyRec;
	}
	public void verifyEmailSubject(WebDriver driver, String expected, String actual){
		WebUtil.waitElement(driver, By.cssSelector("div[class='y6'] span[id] b"), 20);
		WebUtil.isElementDisplayed("Subject is not verified", expected, actual);
		
	}
	public void verifyEmailBody(WebDriver driver, String expected, String actual){
		WebUtil.waitElement(driver, By.cssSelector("div[class='y6'] span[id] b"), 20);
		WebUtil.isElementDisplayed("Body Email is not verified", expected, actual);
	}
	public void newEmailRead(WebDriver driver){
		WebUtil.click(driver, By.cssSelector("div[class='y6'] span[id] b"));
	}
	public void signOut(WebDriver driver){
		WebUtil.click(driver, By.cssSelector("span[class='gb_2a gbii']"));
		WebUtil.click(driver,By.id("gb_71"));
		
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
}
