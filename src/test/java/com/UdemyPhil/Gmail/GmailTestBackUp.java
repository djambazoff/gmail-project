package com.UdemyPhil.Gmail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailTestBackUp {
	WebDriver driver=new FirefoxDriver();
//WebDriver driver=new ChromeDriver();

	@Test
public void SendReceiveEmail(){

	// 1.Go to Gmail website
	
	driver.get("http://gmail.com");
	
	//2. Fill in username
	WebElement usernameField=driver.findElement(By.id("Email"));
	usernameField.clear();
	usernameField.sendKeys("projectwithken");
	WebElement clickNext=driver.findElement(By.id("next"));
	clickNext.click();	
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
	//3. Fill in password
	WebElement passwordField=driver.findElement(By.id("Passwd"));
	passwordField.sendKeys("4504manage");
	
	//4. Click on sign
	WebElement signInButton=driver.findElement(By.id("signIn"));
	signInButton.click();
	
	//5. verify user is logedIn
	WebDriverWait wait = new WebDriverWait(driver, 20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Inbox")));
	Assert.assertTrue("SignIn is not verified",!driver.findElements(By.partialLinkText("Inbox")).isEmpty());	
					
	//6.Click Compose
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='button'][gh='cm']")));
	WebElement composeEmail=driver.findElement(By.cssSelector("div[role='button'][gh='cm']"));
	composeEmail.click();
	
	//7.Fill in recipient
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("textarea[name='to']")));
	WebElement toEmail=driver.findElement(By.cssSelector("textarea[name='to']"));
	toEmail.sendKeys("projectwithken@gmail.com");
	
	//8.Fill in subject  input[name="subjectbox"]
	WebElement subjectEmail=driver.findElement(By.cssSelector("input[name='subjectbox']"));
	subjectEmail.sendKeys("Just a test!");
	
	//9.Fill in email body
	WebElement textEmail=driver.findElement(By.cssSelector("div[role='textbox']"));
	textEmail.sendKeys("Hello Testers!");
	//10.Click Send
	
	WebElement sendEmail=driver.findElement(By.cssSelector("div[data-tooltip*='Send']"));
	sendEmail.click();
	//11.Click Inbox again
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Inbox (1)")));
	WebElement inbox=driver.findElement(By.linkText("Inbox (1)"));
	inbox.click();
	
	//12.Check email
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[class='y6'] span[id] b")));
	WebElement newEmailSub=driver.findElement(By.cssSelector("div[class='y6'] span[id] b"));
	String subjectRec=newEmailSub.getText();
	WebElement newEmailBody=driver.findElement(By.cssSelector("div[class='y6'] span[class='y2']"));
	String bodyRec=newEmailBody.getText().replace('-', ' ').trim();
			
	//13.Verify the email subject and email body is correct
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[class='y6'] span[id] b")));
	Assert.assertEquals("Subject is not verified", "Just a test!", subjectRec);	
	Assert.assertEquals("Body Email is not verified", "Hello Testers!", bodyRec);	
	newEmailSub.click();
	
	//14.Sign out  THAT IS A GOOD WAY OF USING SPAN!!!!!!!!!
	WebElement profileButton=driver.findElement(By.cssSelector("span[class='gb_2a gbii']"));
	profileButton.click();
	WebElement signOut = driver.findElement(By.id("gb_71"));
	signOut.click();
	
	//15.verify user is signOut
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signIn")));
	Assert.assertTrue("SignOut is NOT verified!", driver.findElements(By.id("signIn")).size()>0);
}
@After
public void tearDown() {
	driver.quit();
	
}
}
