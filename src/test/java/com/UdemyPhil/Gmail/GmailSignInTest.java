package com.UdemyPhil.Gmail;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.UdemyPhil.Categories.Critical;
import com.UdemyPhil.Categories.Major;
import com.UdemyPhil.Util.WebUtil;
import com.UdemyPhil.pageObjects.EmailHomePage;
import com.UdemyPhil.pageObjects.SignInPage;

import static org.junit.Assert.assertThat;

import java.util.concurrent.TimeUnit;
public class GmailSignInTest {
	WebDriver driver=new FirefoxDriver();
	//WebDriver driver=new ChromeDriver();
	//@Test
	@Category({com.UdemyPhil.Categories.Critical.class})
	public void signInOutEmail(){
		// 1.Go to Gmail website
				SignInPage signInPage=WebUtil.goToSignInPage(driver);
						
				//2. Fill in username
				signInPage.fillInUsername(driver,"projectwithken");
						
				//3. Fill in password
				signInPage.fillInPassword(driver, "4504manage");
						
				//4. Click on sign
				EmailHomePage emailHomePage = signInPage.clickSignIn(driver);
						
				//5. verify user is logedIn
				emailHomePage.verifySignIn(driver,"SignIn is not verified");
				//6.Sign out  THAT IS A GOOD WAY OF USING SPAN!!!!!!!!!
				emailHomePage.signOut(driver);
						
				//7.verify user is signOut
				signInPage.verifySignOut(driver);
										
	}
	@Category({Major.class})
	@Test
	public void SendReceiveEmail(){
	
		// 1.Go to Gmail website
		SignInPage signInPage=WebUtil.goToSignInPage(driver);
				
		//2. Fill in username
		signInPage.fillInUsername(driver,"projectwithken");
				
		//3. Fill in password
		signInPage.fillInPassword(driver, "4504manage");
				
		//4. Click on sign
		EmailHomePage emailHomePage = signInPage.clickSignIn(driver);
				
		//5. verify user is logedIn
		emailHomePage.verifySignIn(driver,"SignIn is not verified");
								
		//6.Click Compose
		emailHomePage.clickCompose(driver);
				
		//7.Fill in recipient
		emailHomePage.fillInRecipient(driver, "projectwithken@gmail.com");
				
		//8.Fill in subject  input[name="subjectbox"]
		final String expectedSubject="Just a test!";
		emailHomePage.inputSubject(driver,expectedSubject);
				
		//9.Fill in email body
		final String expectedBody="Hello Testers!!";
		emailHomePage.fillInBody(driver,expectedBody);
		
		//10.Click Send
		emailHomePage.sendEmail(driver);
		
		//11.Click Inbox again
		emailHomePage.checkInboxStatus(driver);
				
		//12.Check new email information
		String actualSubject = emailHomePage.getActualSubject(driver);
		String actualBody = emailHomePage.getActualBody(driver);
						
		//13.Verify the email subject and email body is correct
		emailHomePage.verifyEmailSubject(driver, expectedSubject,actualSubject);
		emailHomePage.verifyEmailBody(driver, expectedBody, actualBody);
			
		//13.1 mark new email as read
		emailHomePage.newEmailRead(driver);
				
		//14.Sign out  THAT IS A GOOD WAY OF USING SPAN!!!!!!!!!
		emailHomePage.signOut(driver);
				
		//15.verify user is signOut
		signInPage.verifySignOut(driver);
		
	}
	//This is just a note of change to check on jenkins :)
	@After
	public void tearDown() {
		driver.quit();
		
	}
}
